import AppContainer, {Types} from '../di';
import {inject, injectable} from 'inversify';
import Telegraf from 'telegraf';
import * as BetterQueue from 'better-queue';
import {getLogger} from 'log4js';
import axios from 'axios';
import Currency from '../entities/db/Currency';
import FromToCurrency from '../entities/db/FromToCurrency';
import moment = require('moment');
import {DurationInputArg2} from 'moment';
import {TelegrafContext} from 'telegraf/typings/context';

const ChartJSImage = require('chart.js-image');

const logger = getLogger('GetCurrencyService');

interface IQueue {
  options?;
  status?: string;
}

@injectable()
export default class GetCurrencyService {
  private readonly queue: BetterQueue<IQueue>;

  constructor(@inject(Types.Telegraf) private telegraf: Telegraf<TelegrafContext>) {
    this.queue = new BetterQueue(
      this.handleQueueItem.bind(this),
      {
        afterProcessDelay: 1,
        concurrent: 1,
        batchSize: 1,
        batchDelay: 1
      }
    );
  }

  private async handleQueueItem(message: IQueue, callback: BetterQueue.ProcessFunctionCb<any>) {
    try {
      if (message.status === `getCurrency`) {
        const response = await axios.get(message.options.url);

        let rates = [];

        if (response?.data?.rates) {
          Object.keys(response.data.rates).forEach(key => {
            rates.push({name: key, value: Math.ceil(response.data.rates[key] * 100)})
          });
        }

        return callback(null, rates);
      }
    } catch (err) {
      callback(err);
    }
  }

  private async isNeedUpdate(fromCurrency: string): Promise<boolean | void> {
    try {
      let currency = await Currency.findOne({CurrencyName: fromCurrency});
      return !(currency?.LastDateUpdate && moment(currency.LastDateUpdate).add(10, 'm').isAfter(moment()));
    } catch (err) {
      logger.error('Error for GetCurrencyService in isNeedUpdate', err);
    }
  }

  async getAllCurrencyDB(fromCurrency: string): Promise<FromToCurrency[] | void> {
    try {
      let fromCurrencyDB: Currency | undefined = await Currency.findOne({CurrencyName: fromCurrency});

      if (!(fromCurrencyDB)) {
        return;
      }

      return await FromToCurrency.find({
        join: {
          alias: 'FromToCurrency',
          leftJoinAndSelect: {
            FromCurrencyID: 'FromToCurrency.FromCurrencyID',
            ToCurrencyID: 'FromToCurrency.ToCurrencyID',
          }
        }
      });
    } catch (err) {
      logger.error('Error for GetCurrencyService in isNeedUpdate', err);
    }
  }

  public async getAllCurrency(fromCurrency: string = 'USD'): Promise<FromToCurrency[] | void> {
    try {
      if (await this.isNeedUpdate(fromCurrency)) {
        return await this.getCurrency();
      }

      return await this.getAllCurrencyDB(fromCurrency);
    } catch (err) {
      logger.error('Error for GetCurrencyService in getAllCurrency', err);
    }
  }

  async getCurrency(baseCurrency = 'USD'): Promise<any[]> {
    try {
      let valueCurrency;
      try {
        const url: string = `https://api.exchangeratesapi.io/latest?base=${baseCurrency}`;
        valueCurrency = await new Promise((resolve, reject) => {
          this.queue.push({options: {url}, status: `getCurrency`}, (err, result) => {
            if (err) {
              return reject(err);
            }
            return resolve(result);
          });
        });
      } catch (err) {
        logger.error('Error for GetCurrencyService in getCurrency groups.getMembers', err);
      }
      return await this.saveCurrencyDB(baseCurrency, valueCurrency);
    } catch (err) {
      logger.error('Error for GetCurrencyService in getCurrency', err);
    }
  }

  async saveCurrencyDB(baseCurrency, AllCurrency: { name: string, value: number }[]): Promise<any[]> {
    try {
      if (!AllCurrency.length) {
        return [];
      }

      let resultCurrency = [];
      const lastDateUpdate = moment().toDate();
      let fromCurrencyDB = await Currency.findOne({CurrencyName: baseCurrency});

      if (!fromCurrencyDB) {
        fromCurrencyDB = new Currency();
        fromCurrencyDB.CurrencyName = baseCurrency;
        fromCurrencyDB.LastDateUpdate = lastDateUpdate;
        await fromCurrencyDB.save();
      }

      for (let currency of AllCurrency) {
        let toCurrencyDB = await Currency.findOne({CurrencyName: currency.name});
        if (!toCurrencyDB) {
          const newCurrency = new Currency();
          newCurrency.CurrencyName = currency.name;
          toCurrencyDB = await newCurrency.save();
        }

        let fromToCurrencyDB = await FromToCurrency.findOne({
          where: {
            FromCurrencyID: fromCurrencyDB,
            ToCurrencyID: toCurrencyDB,
          },
          join: {
            alias: 'FromToCurrency',
            leftJoinAndSelect: {
              FromCurrencyID: 'FromToCurrency.FromCurrencyID',
              ToCurrencyID: 'FromToCurrency.ToCurrencyID',
            }
          }
        });

        if (!fromToCurrencyDB) {
          fromToCurrencyDB = new FromToCurrency();
          fromToCurrencyDB.FromCurrencyID = fromCurrencyDB;
          fromToCurrencyDB.ToCurrencyID = toCurrencyDB;
        }

        fromToCurrencyDB.Value = currency.value;
        await fromToCurrencyDB.save();
        resultCurrency.push(fromToCurrencyDB);
      }

      fromCurrencyDB.LastDateUpdate = lastDateUpdate;
      await fromCurrencyDB.save();

      return resultCurrency;
    } catch (err) {
      logger.error('Error for GetCurrencyService in saveCurrencyDB', err);
      return [];
    }
  }

  async exchange(FromCurrency: string, ToCurrency: string, Amount: string): Promise<string> {
    try {
      let fromCurrencyDB: Currency | undefined = await Currency.findOne({CurrencyName: FromCurrency});
      if (!fromCurrencyDB || (await this.isNeedUpdate(FromCurrency))) {
        await this.getCurrency(FromCurrency);
        fromCurrencyDB = await Currency.findOne({CurrencyName: FromCurrency});
      }

      let toCurrencyDB: Currency | undefined = await Currency.findOne({CurrencyName: ToCurrency});

      if (!toCurrencyDB) {
        return;
      }

      const fromToCurrencyDB: FromToCurrency | undefined = await FromToCurrency.findOne({
        FromCurrencyID: fromCurrencyDB,
        ToCurrencyID: toCurrencyDB
      });

      if (!fromToCurrencyDB) {
        return;
      }

      const resultConversion: number = +Amount.replace(',', '.') * fromToCurrencyDB.Value;
      return `${(resultConversion / 100).toFixed(2)} ${ToCurrency}`
    } catch (err) {
      logger.error('Error for GetCurrencyService in exchange', err);
    }
  }

  async history(FromCurrency: string, ToCurrency: string, amount: string, type: DurationInputArg2): Promise<Buffer | void> {
    try {
      try {
        const startAt: string = moment().subtract(amount, type).format('YYYY-MM-DD');
        const endAt: string = moment().format('YYYY-MM-DD');
        const url: string = [
          `https://api.exchangeratesapi.io/history`,
          `?start_at=${startAt}&end_at=${endAt}`,
          `&base=${FromCurrency}&symbols=${ToCurrency}`
        ].join('');

        const response = await axios.get(url);

        if (!response?.data?.rates) {
          return;
        }

        return await this.getChartImage(response.data.rates, FromCurrency, ToCurrency);
      } catch (err) {
        logger.error('Error for GetCurrencyService in getCurrency groups.getMembers', err);
      }
    } catch (err) {
      logger.error('Error for GetCurrencyService in history', err);
    }
  }

  async getChartImage(rates, FromCurrency, ToCurrency): Promise<Buffer | void> {
    try {
      let labels: string[] = [];
      let data: string[] = [];
      Object.keys(rates).forEach(key => {
        labels.push(key);
        data.push(rates[key][ToCurrency]);
      });

      const line_chart = ChartJSImage().chart({
        'type': 'line',
        'data': {
          'labels': labels,
          'datasets': [
            {
              'label': `${FromCurrency} to ${ToCurrency}`,
              'borderColor': 'rgb(255,+99,+132)',
              'backgroundColor': 'rgba(255,+99,+132,+.5)',
              'data': data
            }
          ]
        },
        'options': {
          'scales': {
            'yAxes': [
              {
                'stacked': true,
                'scaleLabel': {
                  'display': true,
                  'labelString': 'Value'
                }
              }
            ]
          }
        }
      })
        .backgroundColor('white')
        .width(500)
        .height(300);

      return await line_chart.toBuffer();
    } catch (err) {
      logger.error('Error for GetCurrencyService in getChartImage', err);
    }
  }
}

AppContainer.bind<GetCurrencyService>(Types.GetCurrencyService).to(GetCurrencyService).inSingletonScope();
