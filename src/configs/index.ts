import DBConfiguring from './DBConfiguring';
import TelegramBotConfiguring from './TelegramBotConfiguring';
import LoggerConfiguring from './LoggerConfiguring';

const functions: Array<Function> = [
  LoggerConfiguring,
  DBConfiguring,
  TelegramBotConfiguring,
];

export default async function Configuring() {
  try {
    for (const func of functions) {
      await func();
    }
  } catch (err) {
    console.error(`Error in config/index.ts`, err);
  }
}
