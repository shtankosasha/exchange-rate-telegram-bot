import config from '../config';
import Telegraf from 'telegraf';
import AppContainer, {Types} from '../di';
import Stages from '../wizards';
import telegrafThrottler from 'telegraf-throttler';
import {TelegrafContext} from 'telegraf/typings/context';

const Session = require('telegraf/session');

export default async function TelegramBotConfiguring() {
  try {
    const bot = new Telegraf(config.TELEGRAM_BOT_TOKEN);

    const throttler = telegrafThrottler();
    bot.use(throttler);

    bot.use(Session());
    bot.use(Stages.middleware());

    bot.telegram.getMe().then((botInfo) => {
      bot.options.username = botInfo.username;
    });

    (bot as any).launch().then(() => {
      console.info('Start bot');
    });

    AppContainer
      .bind<Telegraf<TelegrafContext>>(Types.Telegraf)
      .toConstantValue(bot);
  } catch (err) {
    console.error(`Error in config/TelegramBotConfiguring.ts`, err);
  }
}
