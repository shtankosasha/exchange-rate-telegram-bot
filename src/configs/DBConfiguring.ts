import {createConnection, Logger} from 'typeorm';
import config from '../config';
import {getLogger} from 'log4js';

const logger = getLogger('DBConfiguring');

class DBLogger implements Logger {
  logQuery(query: string, parameters: any[]) {
    if (Array.isArray(parameters) && parameters.length !== 0) {
      logger.debug(`SQL: ${query}; Params: ${parameters}`);
      return;
    }

    logger.debug(`SQL: ${query}`);
  }

  logQueryError(error: string, query: string, parameters: any[]) {
    if (Array.isArray(parameters) && parameters.length !== 0) {
      logger.debug(`Error: ${error}; SQL: ${query}; Params: ${parameters}`);
      return;
    }

    logger.debug(`Error: ${error}; SQL: ${query}`);
  }

  logQuerySlow(time: number, query: string, parameters: any[]) {
    if (Array.isArray(parameters) && parameters.length !== 0) {
      logger.debug(`Slow SQL: ${query}; Params: ${parameters}; Time: ${time}`);
      return;
    }

    logger.debug(`Slow SQL: ${query}; Time: ${time}`);
  }

  logSchemaBuild(message: string) {
    logger.debug(`Build: ${message}`);
  }

  logMigration(message: string) {
    logger.debug(`Migration: ${message}`);
  }

  log(level: string, message: any) {
    if (level === 'log') {
      logger.debug(`TypeORM: ${message}`);
    } else if (level === 'info') {
      logger.info(`TypeORM: ${message}`);
    } else {
      logger.warn(`TypeORM: ${message}`);
    }
  }
}

export default async function DBConfiguring() {
  try {
    await createConnection({
      type: 'postgres',
      url: config.DB_URI,
      logging: 'all',
      logger: new DBLogger(),
      synchronize: true,
      entities: [
        'src/entities/db/**/*.ts'
      ],
    });
  } catch (err) {
    console.error(`Error in config/DBConfiguring.ts`, err);
  }
}
