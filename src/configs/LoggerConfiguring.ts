import config from '../config';
import * as log4js from 'log4js';

export default () => {
  log4js.configure({
    appenders: {
      console: {type: 'console'}
    },
    categories: {
      default: {
        appenders: ['console'],
        level: config.ENV === 'DEV' ? 'debug' : 'info'
      }
    }
  });
};
