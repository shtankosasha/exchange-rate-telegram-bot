import AppContainer, {Types} from '../di';
import GetCurrencyService from '../services/GetCurrencyService';
import {getLogger} from 'log4js';
import FromToCurrency from '../entities/db/FromToCurrency';
import {DurationInputArg2} from 'moment';
import {Markup} from 'telegraf';
import {TelegrafContext} from 'telegraf/typings/context';

const logger = getLogger('CurrencyUtils');

export default class CurrencyUtils {
  private getCurrencyService: GetCurrencyService = AppContainer.get<GetCurrencyService>(Types.GetCurrencyService);

  async list(ctx: TelegrafContext): Promise<any> {
    try {
      let allCurrency: FromToCurrency[] | void = await this.getCurrencyService.getAllCurrency();

      if (!allCurrency || !allCurrency.length) {
        const message: string[] = ['Повторите через пару минут'];
        return await ctx.reply(message.join('\n'));
      }

      let currencyMessage: string[] = [];

      allCurrency.forEach((currency: FromToCurrency) => {
        if (currency.FromCurrencyID.id !== currency.ToCurrencyID.id) {
          currencyMessage.push(`${currency.ToCurrencyID.CurrencyName}: ${(currency.Value / 100).toFixed(2)}`);
        }
      });
      await ctx.reply(currencyMessage.join('\n'));
    } catch (err) {
      logger.error(err);
    }
  }

  async exchange(ctx: TelegrafContext) {
    try {
      let text = ctx.message.text;
      const phraseOnlyTitle = this.characterReplacement(text);
      const commands = phraseOnlyTitle.toUpperCase().split(' ');

      if (commands.length !== 4) {
        const message = ['Проверьте правильность запроса'];
        return await ctx.reply(message.join('\n'));
      }

      let exchangeResult = await this.getCurrencyService.exchange(commands[1], commands[3], commands[0]);

      if (!exchangeResult) {
        const message = ['Нет возможности конвертации таких валют'];
        await ctx.reply(message.join('\n'));
        return;
      }

      await ctx.reply(exchangeResult);
    } catch (err) {
      logger.error(err);
    }
  }

  characterReplacement(str: string): string {
    let newStr = str;
    const symbolNameCurrency = [
      {symbol: /\$/g, name: 'USD'},
    ];

    symbolNameCurrency.forEach(({symbol, name}) => {
      newStr = newStr.replace(symbol, name)
    });
    newStr = newStr
      .replace(/\s+/g, ' ')
      .replace('/exchange ', '')
      .replace('/history ', '');
    return newStr;
  }

  async history(ctx: TelegrafContext): Promise<void> {
    try {
      let text = ctx.message.text;
      const phraseOnlyTitle: string = this.characterReplacement(text).replace('/', ' ');
      const commands: string[] = phraseOnlyTitle.toUpperCase().split(' ');

      if (commands.length !== 5) {
        const message: string[] = ['Проверьте правильность запроса'];
        await ctx.reply(message.join('\n'));
        return;
      }

      let exchangeResult = await this.getCurrencyService.history(commands[0], commands[1], commands[3], commands[4] as DurationInputArg2);

      if (!exchangeResult) {
        const message = 'Для выбранной валюты нет данных об обменном курсе';
        const keyboard = Markup
          .inlineKeyboard([
            Markup.callbackButton('show error', `history_error`),
          ])
          .resize()
          .extra({
            parse_mode: 'HTML'
          });

        await ctx.reply(message, keyboard);
        return;
      }

      await ctx.replyWithPhoto({source: exchangeResult})
    } catch (err) {
      logger.error(err);
    }
  }

  historyError(ctx: TelegrafContext) {
    return ctx.answerCbQuery(`Для выбранной валюты нет данных об обменном курсе`, false)
  }
}
