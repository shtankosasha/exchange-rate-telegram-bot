import * as process from 'process';

interface IConfig {
  ENV: string;
  TELEGRAM_BOT_TOKEN: string;
  DB_URI: string;
}

const config = {
  DEV: {
    ENV: 'DEV',
    TELEGRAM_BOT_TOKEN: process.env.TELEGRAM_BOT_TOKEN,
    DB_URI: 'postgres://evjblscd:0HJRVUwNByNpsKmKAzhA7DElACqfNuZD@hattie.db.elephantsql.com:5432/evjblscd',
  },
  PRODUCTION: {
    ENV: 'PRODUCTION',
    TELEGRAM_BOT_TOKEN: process.env.TELEGRAM_BOT_TOKEN,
    DB_URI: process.env.DB_URI,
  },
  GLOBAL: {
  }
};

const mergedConfig = Object.assign(config[process.env.NODE_ENV || 'DEV'], config.GLOBAL);

export default <IConfig> mergedConfig;
