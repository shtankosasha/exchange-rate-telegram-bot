import {Container} from 'inversify';
import GetCurrencyService from '../services/GetCurrencyService';

const Types = {
  Telegraf: Symbol.for('Telegraf'),
  Logger: Symbol.for('Logger'),
  GetCurrencyService: Symbol.for('GetCurrencyService'),
};

const AppContainer = new Container();

function BindDependencies(func, dependencies): () => () => {} {
  return () => {
    const injections = dependencies.map((dependency) => AppContainer.get(dependency));
    return new func(...injections);
  };
}

export {
  Types,
  BindDependencies
};
export default AppContainer;
