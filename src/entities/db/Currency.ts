import {BaseEntity, Column, Entity, PrimaryGeneratedColumn} from 'typeorm';

@Entity()
export default class Currency extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({nullable: false, unique: true})
  CurrencyName: string;

  @Column('timestamp', { default: null, nullable: true })
  LastDateUpdate: Date;
}
