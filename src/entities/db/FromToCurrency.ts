import {BaseEntity, Column, Entity, ManyToOne, PrimaryGeneratedColumn} from 'typeorm';
import Currency from './Currency';
import {JoinColumn} from 'typeorm/decorator/relations/JoinColumn';

@Entity()
export default class FromToCurrency extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({nullable: false})
  Value: number;

  // @Column({nullable: false})
  // FromCurrencyID: number;
  //
  // @Column({nullable: false})
  // ToCurrencyID: number;

  @ManyToOne(type => Currency)
  @JoinColumn({ referencedColumnName: 'CurrencyName' })
  FromCurrencyID: Currency;

  @ManyToOne(type => Currency)
  @JoinColumn({ referencedColumnName: 'CurrencyName' })
  ToCurrencyID: Currency;

  // @ManyToOne(type => Currency, currency => currency.fromTo)
  // Currency: Currency;
}
