import {BaseEntity, Column, Entity, PrimaryGeneratedColumn} from 'typeorm';

@Entity()
export default class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({unique: true, nullable: true})
  userId: number | null;

  @Column({default: false})
  isBaned: boolean;

  @Column({nullable: true})
  DateBaned: string;

  @Column({nullable: true})
  DatePastMessage: string;
}
