import 'reflect-metadata';

import './services';
import Configuring from './configs';
import SetupRouters from './routers';

(async () => {
  try {
    await Configuring();
    SetupRouters();
  } catch (err) {
    console.error(`Error in src/index.js`, err);
  }
})();
