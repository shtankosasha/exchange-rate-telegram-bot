import User from '../entities/db/User';

export default class StartController {
  static async onStart(ctx): Promise<void> {
    let user = new User();
    user.userId = ctx.from.id;

    let userId = await User.findOne(user);

    if (!userId) {
      await user.save();
      await ctx.reply([
        'Добро пожаловать!',
        '/list – ',
        '/exchange from to',
        '/history from/to for * days',
      ].join('\n'));
      return;
    }

    await ctx.reply([
      'Вы уже зарегистрированы в системе.',
        '/list – ',
        '/exchange from to',
        '/history from/to for * days',
    ].join('\n'));
  }
}
