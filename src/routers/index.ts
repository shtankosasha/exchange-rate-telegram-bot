import StartRouter from './StartRouter';

const routers = [
  StartRouter,
];

export default () => {
  try {
    for (const router of routers) {
      router();
    }
  } catch (err) {
    console.error(`Error in router/index.js`, err);
  }
}
