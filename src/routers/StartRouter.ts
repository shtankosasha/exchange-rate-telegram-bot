import {BindDependencies, Types} from '../di';
import Telegraf, {Markup} from 'telegraf';
const Extra = require('telegraf/extra');
import StartController from '../controllers/StartController';
import CurrencyUtils from '../utils/CurrencyUtils';
import {interfaces} from 'inversify';
import {TelegrafContext} from 'telegraf/typings/context';

function StartRouter(telegraf: Telegraf<TelegrafContext>) {

  const currencyUtils = new CurrencyUtils();

  telegraf.command('start', ctx => StartController.onStart(ctx));

  telegraf.command('list', ctx => currencyUtils.list(ctx));
  telegraf.hears(/^\/exchange /, ctx => currencyUtils.exchange(ctx));
  telegraf.hears(/^\/history /, ctx => currencyUtils.history(ctx));

  telegraf.action('history_error', ctx => currencyUtils.historyError(ctx))
}

export default BindDependencies(StartRouter, [Types.Telegraf]);
