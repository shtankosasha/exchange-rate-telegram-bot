# exchange-rate-telegram-bot
Telegram bot

1. ###### Получить список
/list

2. ###### Получить график обменного курса
    /history */* for * days
  
    **Пример:**
    /history USD/CAD for 7 days

3. ###### Произвести расчет конвертации
    /exchange * * to *

    **Пример:**
    /exchange 10 USD to CAD
